from django.contrib import admin
from .models import Topic, Entry
# Register your models here.

class EntryTopic(admin.TabularInline):
    model = Entry
    extra = 1 

class TopicAdmin(admin.ModelAdmin):
    list_display = ('title', 'author', 'genre')
    list_filter = ('title', 'author')
    inlines = [EntryTopic]
    

admin.site.register(Topic, TopicAdmin)