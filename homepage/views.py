from django.shortcuts import render, redirect 
from django.forms import inlineformset_factory
from django.contrib.auth.models import User 
from .models import Topic, Entry
from .forms import TopicForm, EntryForm
# Create your views here.

def home(request):
    topics = Topic.objects.all()
    context = {'topics': topics}
    return render(request, 'homepage/home.html', context)

def entry(request, t_id):
    topics = Topic.objects.get(id=t_id)
    texts = topics.entry_set.all()
    context = {'texts':texts}
    return render(request, 'homepage/entry.html', context)

def new_topic(request):
    if request.method != 'POST': 
        form = TopicForm({'author': request.user.id} or None) 
    else:
        form = TopicForm(data=request.POST)
        if form.is_valid():
            form.save()
            return redirect('users:new_entry')
    context = {'form':form}
    return render(request, 'homepage/postform.html', context)

def new_entry(request, the_id):
    topic = Topic.objects.get(id=the_id)
    if request.method != 'POST': 
        form = EntryForm({'topic': topic.id})
    else:
        form = EntryForm(data=request.POST)
        if form.is_valid():
            form.save()
            return redirect('homepage:home')
    context = {'form':form, 'topic':topic}
    return render(request, 'homepage/new_entry.html', context)

