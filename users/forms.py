from django import forms
from .models import Profile 

class SignUpForm(forms.Form):
    firstname = forms.CharField(max_length=190)
    lastname = forms.CharField(max_length=190)
    username = forms.CharField(max_length=190)
    password = forms.CharField(widget=forms.PasswordInput)


class SignInForm(forms.Form):
    username = forms.CharField(max_length=190)
    password = forms.CharField(widget=forms.PasswordInput)


class ProfileForm(forms.ModelForm):
    class Meta:
        model = Profile
        fields = ['person', 'age', 'description', 'genre']

        widgets = {
            'person' : forms.TextInput(attrs={'type':'hidden'}),
        }

    

        