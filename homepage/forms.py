from django import forms 
from django.forms import  Textarea, inlineformset_factory
from .models import Topic, Entry

class TopicForm(forms.ModelForm):
    class Meta:
        model = Topic 
        fields = '__all__'

        widgets = {
            'author' : forms.TextInput(attrs={'type':'hidden'}),
        }

class EntryForm(forms.ModelForm):
    class Meta:
        model = Entry 
        fields = '__all__'

        widgets = {
            'topic' : forms.TextInput(attrs={'type':'hidden','style':'width: 5em'}),
        }

    