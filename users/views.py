from django.shortcuts import render, redirect, reverse
from django.contrib.auth.models import User 
from homepage.models import Topic
from django.http import Http404
from django.contrib.auth import authenticate, login, logout
from .forms import SignUpForm, SignInForm, ProfileForm
from .models import Profile
# Create your views here.
def signup(request):
    if request.method != 'POST':
        form = SignUpForm()
    else:
        firstname = request.POST['firstname']
        lastname = request.POST['lastname']
        username = request.POST['username']
        password = request.POST['password']
        form = SignUpForm(data=request.POST)
        if form.is_valid():
            user = User.objects.create_user(first_name=firstname, last_name=lastname, username=username, password=password)
            user.save()
            return redirect('users:signin')
        else:
            return reverse('users:signup')
    context = {'form': form}
    return render(request, 'users/signup.html', context)

def signin(request):
    if request.method != 'POST':
        form = SignInForm()
    else:
        username = request.POST['username']
        password = request.POST['password']
        form = SignInForm(data=request.POST)
        if form.is_valid():
            user = authenticate(username=username, password=password)
            if user is not None:
                login(request, user)
                return redirect('users:profile')
            else:
                form = SignInForm(data=form.errors)
        else:
            raise Http404
    context = {'form': form}
    return render(request, 'users/signin.html', context)

def signout(request):
    logout(request)
    return render(request, 'users/success.html')

def profile(request):
    if request.method != 'POST':
        form = ProfileForm({'person':request.user.id})
    else:
        form = ProfileForm(data=request.POST)
        if form.is_valid():
            form.save()
            return redirect('homepage:home')
    context = {'form': form}
    return render(request, 'users/profile.html', context)

def doneprofile(request, username):
    userprofile = User.objects.get(username=username)
    profiles = userprofile.profile
    context = {'userprofile': userprofile, 'profiles':profiles}
    return render(request, 'users/myprofile.html', context)