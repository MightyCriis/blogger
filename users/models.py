from django.db import models
from django.core.validators import MinValueValidator
from django.contrib.auth.models import User 
# Create your models here.

class Profile(models.Model):
    GEN = [
        ('FASHION','Fashion'),
        ('TECHNOLOGY','Technology'),
        ('EDUCATION','Education'),
        ('FINANCE','Finance'),
        ('LIFESTYLE','Lifestyle'),
        ('OTHER','Other'),
    ]
    person = models.OneToOneField(User, on_delete=models.CASCADE)
    age = models.IntegerField(default=13, validators=[MinValueValidator(13)])
    description = models.TextField()
    genre = models.CharField(max_length=40, choices=GEN)

    def __str__(self):
        return self.description