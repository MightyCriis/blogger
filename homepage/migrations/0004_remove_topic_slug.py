# Generated by Django 3.0.8 on 2020-07-15 15:53

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('homepage', '0003_topic_slug'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='topic',
            name='slug',
        ),
    ]
