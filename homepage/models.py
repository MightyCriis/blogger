from django.db import models
from django.contrib.auth.models import User 
# Create your models here.
class Topic(models.Model):
    GEN_TYPE = [
        ('FASHION','Fashion'),
        ('TECHNOLOGY','Technology'),
        ('EDUCATION','Education'),
        ('FINANCE','Finance'),
        ('LIFESTYLE','Lifestyle'),
        ('OTHER','Other'),
    ]
    title = models.CharField(max_length=190)
    author = models.ForeignKey(User, on_delete=models.CASCADE)
    genre = models.CharField(max_length=190, choices=GEN_TYPE)
    

    def __str__(self):
        return self.title

class Entry(models.Model):
    topic = models.ForeignKey(Topic, on_delete=models.CASCADE)
    text = models.TextField()

    def __str__(self):
        return self.text 