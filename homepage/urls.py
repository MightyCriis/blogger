from django.urls import path
from . import views 

app_name = 'homepage'

urlpatterns = [
    path('', views.home, name='home'),
    path('new_topic/', views.new_topic, name='new_topic'),
    #path('article/', views.article, name='article'),
    #path('<int:entry_id>/edit_entry/', views.edit_entry, name='edit_entry'),
    path('<int:the_id>/new_entry/', views.new_entry, name='new_entry'),
    path('topic/<int:t_id>/', views.entry, name='entry'),
]